import express from 'express';
import { Controller } from '@/controllers';
import { errorMiddleware } from '@/middlewares';
import { DBHandler } from '@/config';

export default class App {
  public app: express.Application;
  public port: string | number;

  constructor(controllers: Controller[]) {
    this.app = express();
    this.port = 3000;

    this.connectToTheDatabase();
    this.initializeMiddleWares();
    this.initializeControllers(controllers);
    this.initializeErrorHandling();
  }

  public listen(): void {
    this.app.listen(this.port, () =>
      console.log(`App listening on the port ${this.port}`.blue)
    );
  }

  private initializeMiddleWares() {
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }

  private initializeControllers(controllers: Controller[]) {
    controllers.forEach((controller) => {
      this.app.use('/', controller.router);
    });
  }

  private async connectToTheDatabase() {
    try {      
      const dbHandler = new DBHandler();
      await dbHandler.connect();
      console.log('db is connect'.green);
    } catch (error) {
      console.error(error);
    }
  }
}
