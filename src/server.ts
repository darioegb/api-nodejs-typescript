import 'module-alias/register';
import 'dotenv/config';
import { IndexController, UserController, AuthController } from './controllers';
import { App } from '@/config';
import 'colors';
import { validateEnv } from '@/utils';

validateEnv();

const app = new App([
    new IndexController(),
    new UserController(),
    new AuthController(),
]);

app.listen();
   