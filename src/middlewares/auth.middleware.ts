import { NextFunction, Response } from 'express';
import { HttpException } from '@/exceptions';
import {
  DataStoreInToken,
  RequestWithUser
} from '../data/interfaces/auth.iterface';
import { verify } from 'jsonwebtoken';
import { userModel } from '../data/models/user.model';
import { Roles } from '@/data/enums';

export async function authMiddleware(
  req: RequestWithUser,
  _res: Response,
  next: NextFunction
) {
  const headers = req.headers;
  if (headers && headers.authorization) {
    const secret: string = process.env.JWT_SECRET;
    try {
      let token = headers.authorization;
      if (token.startsWith('Bearer ')) {
        token = token.slice(7, token.length);
      }
      const verificationResponse = <DataStoreInToken>verify(token, secret);
      const _id = verificationResponse._id;
      const findUser = await userModel.findById({ _id }).exec();
      if (findUser) {
        req.user = findUser;
        next();
      } else {
        next(new HttpException(401, 'Wrong authentication token'));
      }
    } catch (_error) {
      next(new HttpException(401, 'Wrong authentication token'));
    }
  } else {
    next(new HttpException(404, 'Authentication token missing'));
  }
}

export async function isAdminMiddleware(
  req: RequestWithUser,
  _res: Response,
  next: NextFunction
) {
  req.user.role !== Roles.Admin
    ? next(new HttpException(403, 'You have no privilege to do that'))
    : next();
}

export async function isAdminOrSameUserMiddleware(
  req: RequestWithUser,
  _res: Response,
  next: NextFunction
) {
  const findUser = req.user;
  const id = req.params.id;

  findUser.role === Roles.Admin || findUser._id.toString() == id
    ? next()
    : next(new HttpException(403, 'You have no privilege to do that'));
}
