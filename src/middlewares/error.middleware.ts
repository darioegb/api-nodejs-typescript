import { NextFunction, Request, Response } from "express";
import { HttpException } from "@/exceptions";

export function errorMiddleware(
    error: HttpException,
    _req: Request,
    res: Response,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    _next: NextFunction
) {
    const status: number = error.status || 500;
    const message: string = error.message || 'Something went wrong';

    res.status(status).json({ message });
}
