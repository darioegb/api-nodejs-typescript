import { NextFunction, Request, RequestHandler, Response } from 'express';
import { HttpException } from '@/exceptions';
import { validate, ValidationError } from 'class-validator';
import { plainToClass } from 'class-transformer';

export function validationMiddleware<T>(
  type: T,
  skipMissingProperties = false
): RequestHandler {
  return (req, _res, next) => {
    validate(plainToClass(<never>type, req.body), {
      skipMissingProperties,
    }).then((errors: ValidationError[]) => {
      if (errors.length > 0) {
        const message = generateMessageFromErros(errors);
        next(new HttpException(400, message));
      } else {
          next();
      }
    });
  };

  function generateMessageFromErros(errors: ValidationError[]) {
    return errors
      .map((error: ValidationError) =>
        Object.values(error.constraints ? error.constraints : [])
      )
      .join(', ');
  }
}
