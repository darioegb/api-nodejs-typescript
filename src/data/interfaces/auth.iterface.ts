import { Request } from 'express';
import { User } from './user.interface';

export interface DataStoreInToken {
    _id: string;
}

export interface UserToken {
    token: string;
    findUser: User;
}

export interface RequestWithUser extends Request {
    user: User;
}