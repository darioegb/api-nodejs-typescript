import BaseCrudService from './baseCrudService.abstract';
import { UserDto } from '@/data/dtos';
import { User } from '@/data/interfaces';
import { userModel } from '@/data/models';
import { HttpException } from '@/exceptions';
import { hash } from 'bcryptjs';

export class UserService extends BaseCrudService<UserDto, User> {
  
  constructor() {
    super();
    this.model = userModel;
  }

  public async createUser(
    userData: UserDto | UserDto[]
  ): Promise<User | User[]> {
    if (userData instanceof Array) {
      userData.forEach(async (item) => {
        await this.checkUserExist(item);
        await this.hashUserPassword(item);
      });
    } else {
      await this.checkUserExist(userData);
      await this.hashUserPassword(userData);
    }
    return await this.create(userData);
  }

  public async findUnserById(userId: string, filter?: string | undefined): Promise<User> {
    try {
      return await this.findEntityById(userId, filter);
    } catch (error) {
      throw new HttpException(409, `You're not user`);
    }
  }

  private async checkUserExist(userData: UserDto) {
    const emailExist: boolean = await this.model.exists({
      email: userData.email,
    });
    if (emailExist) {
      throw new HttpException(
        409,
        `You're email ${userData.email} already exists`
      );
    }
  }

  private async hashUserPassword(userData: UserDto) {
    userData.password = await hash(userData.password, 10);
  }
}
