import { sign } from 'jsonwebtoken';
import BaseCrudService from './baseCrudService.abstract';
import { UserDto } from '@/data/dtos';
import { DataStoreInToken, User, UserToken } from '@/data/interfaces';
import { userModel } from '@/data/models';
import { HttpException } from '@/exceptions';
import { compare } from 'bcryptjs';
import { validateObjectData } from '@/utils';

export class AuthService extends BaseCrudService<UserDto, User> {
  constructor() {
    super();
    this.model = userModel;
  }

  public async login(
    userData: UserDto
  ): Promise<UserToken> {
    validateObjectData(userData, 'UserData');
    const findUser = await this.model.findOne({ email: userData.email }).exec();
    if (!findUser) {
      throw new HttpException(409, `You're email ${userData.email} not found`);
    }
    const isPasswordMatching: boolean = await compare(userData.password, findUser.get('password', null, { getters: false }));

    if(!isPasswordMatching) {
      throw new HttpException(409, `You're password not matching`);
    }
    const token = this.createToken(findUser);
    return { token, findUser };
  }

  public createToken(user: User): string {
    const dataStoreInToken: DataStoreInToken = { _id: user._id };
    const secret: string = process.env.JWT_SECRET;
    const expiresIn = '1h';

    return sign(dataStoreInToken, secret, { expiresIn })
  }
}
